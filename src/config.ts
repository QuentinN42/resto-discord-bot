import { config as load_dotenv } from "dotenv"


export class Env {
    token: string
    guild: string
    channel: string

    constructor() {
        load_dotenv();

        if (!process.env.TOKEN) {
            throw new Error("TOKEN is not defined");
        }
        this.token = process.env.TOKEN;

        if (!process.env.CHANNEL) {
            throw new Error("CHANNEL is not defined");
        }
        this.channel = process.env.CHANNEL;

        if (!process.env.GUILD) {
            throw new Error("GUILD is not defined");
        }
        this.guild = process.env.GUILD;
    }
}

import { Env } from "./config"
import { Bot } from "./bot"
import { Browser } from "./browser"


const env = new Env();
const browser = new Browser();
const bot = new Bot(env, browser);

bot.start();

console.log("init");

import { chromium, Browser as PWBrowser, Page } from 'playwright';
import { Restorant } from "./restorant"
import fetch from 'node-fetch';

export class Browser {
    browser: Promise<PWBrowser>
    url: string

    constructor() {
        this.browser = chromium.launch();
        this.url = "https://www.google.com/maps/search/Restaurants/@48.8672969,2.3467392,17.58z/data=!4m5!2m4!5m2!2e1!4e9!6e5";
    }

    async getPage(url: string): Promise<Page> {
        const browser = await this.browser;
        const page = await browser.newPage();
        await page.goto(url);
        return page;
    }

    async getRestos(): Promise<Array<Restorant>> {
        const page = await this.getPage(this.url);
        const content = await page.locator(`[aria-label='Résultats pour "Restaurants"']`).first().innerHTML();
        await page.close();
        const arr = content.split("<a ");
        const res: Array<Restorant> = [];
        for (const line of arr.slice(1,arr.length)){
            const a = line.split(">")[0];
            const name = a.split("aria-label=\"")[1].split("\"")[0];
            const url = a.split("href=\"")[1].split("\"")[0];
            res.push(new Restorant(name, url));
        }
        console.log(`Returning ${res.length} restorants`);
        return res;
    }
    async short_url(url: string) {
        const form = new URLSearchParams();
        form.append("u", url);
        const result = await fetch("https://www.shorturl.at/shortener.php", {
            method: "post",
            headers: {
               'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: form,
        });
        const txt = await result.text();
        const short = txt.split("id=\"shortenurl\"")[1].split("value=\"")[1].split("\"")[0];
        return `https://${short}`;
    }
}

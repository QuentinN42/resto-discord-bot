import { Env } from "./config"
import { Browser } from "./browser"
import { Client, Intents, Message, TextChannel } from "discord.js"


export class Bot {
    env: Env
    client: Client
    browser: Browser

    constructor(environment: Env, browser: Browser) {
        this.env = environment;
        this.browser = browser;
        this.client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });

        this.client.once("ready", async () => {
            const user = this.client.user;
            if (user)
                console.log(`Logged in as ${user.tag}!`);
            else
                console.log("User is null");
        })

        this.registerActions();
    }

    registerActions() {
        this.client.on('messageCreate', this.processMessage.bind(this));
    }

    async processMessage(message: Message) {
        if (message.author.bot) {
            return;
        }
        if (!this.messageInValidChannel(message)) {
            return;
        }
        await this.deleteLastMessages(message, 100);
        const res = await this.browser.getRestos();
        res.forEach(r => {
            message.channel.send(`${r.name}\n${r.url}`);
        });
    }

    start() {
        this.client.login(this.env.token);
    }

    messageInValidChannel(message: Message) {
        return message.guildId === this.env.guild && message.channelId == this.env.channel;
    }

    async deleteLastMessages(message: Message, number: number) {
        if (!message.guild) {return;}
        const channel = message.guild.channels.cache.get(message.channelId);
        if (! (channel instanceof TextChannel)) {return;}
        const messages = await channel.messages.fetch({ limit: number });
        for (const message of messages.values()) {
            message.delete();
        }
    }
}

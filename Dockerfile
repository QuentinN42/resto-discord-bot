FROM mcr.microsoft.com/playwright:focal
WORKDIR /app

COPY package*.json ./
RUN npm ci

COPY src/ src/
RUN npm run build

CMD npm run start:prod
